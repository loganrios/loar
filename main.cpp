#include <SDL2/SDL.h>
#include <GL/glew.h>

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "gl_util.h"

GLuint load_shader_program(const char* vs_path, const char* fs_path) {
  GLuint vs = gl_shader(GL_VERTEX_SHADER, vs_path);
  GLuint fs = gl_shader(GL_FRAGMENT_SHADER, fs_path);

  const GLuint* shaders = new GLuint[2]{vs, fs};
  GLuint sp = gl_shader_program(2, shaders);
  return sp;
}

int main() {
  if(!gl_log_restart()) {
    return -1;
  }

  if(SDL_Init(SDL_INIT_VIDEO) < 0) {
    gl_log_err("SDL could not initialize.\n");
    return -1;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);

  uint16_t win_width = 1000;
  uint16_t win_height = 1000;

  SDL_Window* window =
    SDL_CreateWindow("loar",
                     SDL_WINDOWPOS_UNDEFINED,
                     SDL_WINDOWPOS_UNDEFINED,
                     win_width,
                     win_height,
                     SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

  if(window == NULL) {
    gl_log_err("Window could not be created.\n");
    return -1;
  }

  SDL_GLContext gl_context = SDL_GL_CreateContext(window);
  if(gl_context == NULL) {
    gl_log_err("Graphics context could not be created.\n");
    return -1;
  }

  glEnable(GL_DEPTH_TEST);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glewExperimental = GL_TRUE;
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // culling MAYBE OH NO
  glEnable(GL_CULL_FACE);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW);

  if(SDL_GL_SetSwapInterval(1) < 0) {
    gl_log_err("Could not set swap interval.\n");
  }

  if(glewInit() != GLEW_OK) {
    gl_log_err("GLEW failed to initialize.\n");
    return -1;
  }

  const GLubyte* renderer = glGetString(GL_RENDERER);
  const GLubyte* version = glGetString(GL_VERSION);
  gl_log("Renderer: %s\n", renderer);
  gl_log("Version: %s\n", version);
  gl_log_params();

  // tell GL to only draw onto a pixel
  // if the shape is closer to the viewer
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);

  //
  // begin main graphics
  //

  GLfloat points[] = {
    -0.5f, -0.5f, 0.0f,
    0.5f, -0.5f, 0.0f,
    0.0f, 0.5f, 0.0f
  };

  GLuint vbo = 0;
  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

  GLfloat colors[] = {
    1.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 1.0f
  };

  GLuint colors_vbo = 0;
  glGenBuffers(1, &colors_vbo);
  glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(colors), colors, GL_STATIC_DRAW);

  float matrix[] = {
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.5f, 0.0f, 0.0f, 1.0f
  };

  GLuint vao = 0;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  glEnableVertexAttribArray(0);
  glEnableVertexAttribArray(1);

  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

  glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);

  GLuint sp = load_shader_program("./test.vert", "./test.frag");

  bool quit = false;
  SDL_Event e;

  int frame_count;
  uint64_t curr_ticks;
  uint64_t prev_ticks;
  uint64_t elapsed_ticks;

  // translation back-and-forth
  float speed = 1.0f; // move at 1 unite per second
  float last_position = 0.0f;

  while(!quit) {
    while(SDL_PollEvent(&e)) {
      if (e.type == SDL_KEYUP && e.key.keysym.sym == 'q') {
        quit = true;
      }
      if (e.type == SDL_KEYUP && e.key.keysym.sym == 'r') {
        sp = load_shader_program("./test.vert", "./test.frag");
      }
    }

    curr_ticks = SDL_GetTicks64();
    elapsed_ticks = curr_ticks - prev_ticks;
    if (elapsed_ticks > 250) {
      prev_ticks = curr_ticks;
      char tmp[128];
      double fps = (double) frame_count / (double) (elapsed_ticks / 1000.0f);
      sprintf(tmp, "opengl @ fps: %.2f", fps);
      SDL_SetWindowTitle(window, tmp);
      frame_count = 0;
    }
    frame_count++;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glViewport(0, 0, win_width, win_height);

    int matrix_location = glGetUniformLocation(sp, "matrix");

    glUseProgram(sp);
    glUniformMatrix4fv(matrix_location, 1, GL_FALSE, matrix);

    glBindVertexArray(vao);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    SDL_GL_SwapWindow(window);
  }

  //
  // end main graphics
  //

  SDL_GL_DeleteContext(gl_context);
  gl_context = NULL;
  SDL_DestroyWindow(window);
  window = NULL;
  SDL_Quit();

  return 0;
}
