#ifndef GL_UTIL_H
#define GL_UTIL_H

#include <stdlib.h>
#include <stdarg.h>

#include <GL/glew.h>

//
// LOG
//

#define GL_LOG_FILE "gl.log"

bool gl_log_restart(void);

bool gl_log(const char* msg, ...);

bool gl_log_err(const char* msg, ...);

void gl_log_params(void);

//
// SHADER
//

GLuint gl_shader(GLenum type, const char* source);

GLuint gl_shader_program(size_t count, const GLuint* shaders);

void gl_shader_program_print_all(GLuint id);

bool gl_shader_program_is_valid(GLuint id);

#endif // GL_UTIL_H
