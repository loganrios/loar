TARGET := loar

CC := g++
VER := -std=c++20
CXXFLAGS := -Wall -Wextra -pedantic
LDFLAGS := -lGLEW -lGL -lSDL2

OBJS := main.o \
	gl_util.o \
  gfx_math.o

.PHONY: run

all: $(OBJS)
	$(CC) $(OBJS) $(CXXFLAGS) $(LDFLAGS) -o $(TARGET) $(VER)

run: all
	./$(TARGET)

gl_util.o: gl_util.h gl_util.c
	$(CC) $(CXXFLAGS) -c gl_util.c -o $@
gfx_math.o: gfx_math.hpp gfx_math.cpp
	$(CC) $(CXXFLAGS) -c gfx_math.cpp -o $@
main.o: main.cpp
	$(CC) $(CXXFLAGS) -c main.cpp -o $@

clean:
	rm -f $(OBJS) $(TARGET)
