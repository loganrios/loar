#include "gl_util.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

//
// LOG
//

bool gl_log_restart(void) {
  FILE* file = fopen(GL_LOG_FILE, "w");
  if(!file) {
    fprintf(stderr,
            "ERROR: could not open GL_LOG_FILE %s for writing\n",
            GL_LOG_FILE);
    return false;
  }

  time_t now = time(NULL);
  char* date = ctime(&now);
  fprintf(file, "GL_LOG_FILE log. local time %s\n", date);
  fclose(file);
  return true;
}

bool gl_log(const char* msg, ...) {
  va_list argptr;
  FILE* file = fopen(GL_LOG_FILE, "a");
  if(!file) {
    fprintf(stderr,
            "ERROR: could not open GL_LOG_FILE %s for appending\n",
            GL_LOG_FILE);
    return false;
  }
  va_start(argptr, msg);
  vfprintf(file, msg, argptr);
  va_end(argptr);
  fclose(file);
  return true;
}

bool gl_log_err(const char* msg, ...) {
  va_list argptr;
  FILE* file = fopen(GL_LOG_FILE, "a");
  if(!file) {
    fprintf(stderr,
            "ERROR: could not open GL_LOG_FILE %s for appending\n",
            GL_LOG_FILE);
    return false;
  }

  va_start(argptr, msg);
  vfprintf(file, msg, argptr);
  va_end(argptr);

  va_start(argptr, msg);
  vfprintf(stderr, msg, argptr);
  va_end(argptr);

  fclose(file);

  return true;
}

void gl_log_params(void) {
  GLenum params[] = {
    GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS,
    GL_MAX_CUBE_MAP_TEXTURE_SIZE,
    GL_MAX_DRAW_BUFFERS,
    GL_MAX_FRAGMENT_UNIFORM_COMPONENTS,
    GL_MAX_TEXTURE_IMAGE_UNITS,
    GL_MAX_TEXTURE_SIZE,
    GL_MAX_VARYING_FLOATS,
    GL_MAX_VERTEX_ATTRIBS,
    GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS,
    GL_MAX_VERTEX_UNIFORM_COMPONENTS,
    GL_MAX_VIEWPORT_DIMS,
    GL_STEREO,
  };
  const char* names[] = {
    "GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS",
    "GL_MAX_CUBE_MAP_TEXTURE_SIZE",
    "GL_MAX_DRAW_BUFFERS",
    "GL_MAX_FRAGMENT_UNIFORM_COMPONENTS",
    "GL_MAX_TEXTURE_IMAGE_UNITS",
    "GL_MAX_TEXTURE_SIZE",
    "GL_MAX_VARYING_FLOATS",
    "GL_MAX_VERTEX_ATTRIBS",
    "GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS",
    "GL_MAX_VERTEX_UNIFORM_COMPONENTS",
    "GL_MAX_VIEWPORT_DIMS",
    "GL_STEREO",
  };
  gl_log( "GL Context Params:\n" );
  // integers - only works if the order is 0-10 integer return types
  for ( int i = 0; i < 10; i++ ) {
    int v = 0;
    glGetIntegerv( params[i], &v );
    gl_log( "%s %i\n", names[i], v );
  }
  // others
  int v[2];
  v[0] = v[1] = 0;
  glGetIntegerv( params[10], v );
  gl_log( "%s %i %i\n", names[10], v[0], v[1] );
  unsigned char s = 0;
  glGetBooleanv( params[11], &s );
  gl_log( "%s %i\n", names[11], (unsigned int)s );
  gl_log( "-----------------------------\n" );
}

//
// SHADER
//

char* slurp(const char* filename) {
  FILE* file = fopen(filename, "r");
  if (file == NULL) {
      printf("Could not open file %s\n",  filename);
      return NULL;
  }

  fseek(file, 0, SEEK_END);
  long length = ftell(file);
  fseek(file, 0, SEEK_SET);

  char* buffer = (char*) malloc(length + 1);
  if (buffer == NULL) {
    printf("Could not allocate memory for file %s\n",  filename);
    free(buffer);
    fclose(file);
    return NULL;
  }

  fread(buffer, 1, length, file);
  buffer[length] = '\0';

  fclose(file);
  return buffer;
}

void gl_shader_print_info(GLuint id) {
  int max_length = 2048;
  int actual_length = 0;
  char log[2048];
  glGetShaderInfoLog(id, max_length, &actual_length, log);
  gl_log("Shader info log for GL index %u:\n%s\n", id, log);
}

GLuint gl_shader(GLenum type, const char* filepath) {
  GLuint id = glCreateShader(type);
  char* source = slurp(filepath);
  gl_log("Shader source for %d:\n%s", id, source);
  glShaderSource(id, 1, &source, NULL);
  glCompileShader(id);
  int params = -1;
  glGetShaderiv(id, GL_COMPILE_STATUS, &params);
  if(GL_TRUE != params) {
    gl_log_err("ERROR: GL shader index %i did not compile\n", id);
    gl_shader_print_info(id);
    return 0;
  }

  return id;
}

void gl_shader_program_print_info(GLuint id) {
  int max_length = 2048;
  int actual_length = 0;
  char log[2048];
  glGetProgramInfoLog(id, max_length, &actual_length, log);
  gl_log("Program info log for GL index %u\n");
}

GLuint gl_shader_program(size_t count, const GLuint* shaders) {
  if(shaders == NULL) {
    gl_log_err("ERROR: shader program must have non-NULL shaders\n");
    return 0;
  }

  GLuint id = glCreateProgram();
  if(id == 0) {
    gl_log_err("ERROR: could not create shader program.\n");
    return 0;
  }

  for(GLuint i = 0; i < count; i++) {
    glAttachShader(id, shaders[i]);
  }

  glLinkProgram(id);
  int params = -1;
  glGetProgramiv(id, GL_LINK_STATUS, &params);
  if(GL_TRUE != params) {
    gl_log_err("ERROR: could not link shader program GL index %u\n", id);
    gl_shader_program_print_info(id);
    return 0;
  }

  return id;
}

const char* gl_type_to_string(GLenum type) {
  switch(type) {
  case GL_BOOL: return "bool";
  case GL_INT: return "int";
  case GL_FLOAT: return "float";
  case GL_FLOAT_VEC2: return "vec2";
  case GL_FLOAT_VEC3: return "vec3";
  case GL_FLOAT_VEC4: return "vec4";
  case GL_FLOAT_MAT2: return "mat2";
  case GL_FLOAT_MAT3: return "mat3";
  case GL_FLOAT_MAT4: return "mat4";
  case GL_SAMPLER_2D: return "sampler2D";
  case GL_SAMPLER_3D: return "sampler3D";
  case GL_SAMPLER_CUBE: return "samplerCube";
  case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
  default: break;
  }
  return "other";
}

void gl_shader_program_print_all(GLuint id) {
  gl_log("\nShader Program %i info:\n", id);
  int params = -1;
  glGetProgramiv(id, GL_LINK_STATUS, &params);
  gl_log("GL_LINK_STATUS = %i\n", params);

  glGetProgramiv(id, GL_ATTACHED_SHADERS, &params);
  gl_log("GL_ATTACHED_SHADERS = %i\n", params);

  glGetProgramiv(id, GL_ACTIVE_ATTRIBUTES, &params);
  gl_log("GL_ACTIVE_ATTRIBUTES = %i\n", params);

  for(GLuint i = 0; i < (GLuint)params; i++) {
    char name[64];
    int max_length = 64;
    int actual_length = 0;
    int size = 0;
    GLenum type;
    glGetActiveAttrib(id, i, max_length, &actual_length, &size, &type, name);

    if(size > 1) {
      for(int j = 0; j < size; j++) {
        char long_name[64];
        // TODO throws a GCC warning
        // because truncation may not be desired.
        // it's silly, but we should fix it eventually.
        snprintf(long_name, 64, "%s[%i]", name, j);
        int location = glGetAttribLocation(id, long_name);
        gl_log(" %i | type:%s name:%s location:%i\n", i,
               gl_type_to_string(type), long_name, location);
      }
    } else {
      int location = glGetAttribLocation(id, name);
      gl_log(" %i | type:%s name:%s location:%i\n", i,
             gl_type_to_string(type), name, location);
    }
  }

  glGetProgramiv(id, GL_ACTIVE_UNIFORMS, &params);
  gl_log("GL_ACTIVE_UNIFORMS = %i\n", params);

  for(GLuint i = 0; i < (GLuint)params; i++) {
    char name[64];
    int max_length = 64;
    int actual_length = 0;
    int size = 0;
    GLenum type;
    glGetActiveUniform(id, i, max_length, &actual_length, &size, &type, name);
    if(size > 1) {
      for(int j = 0; j < size; j++) {
        char long_name[64];
        // TODO throws a GCC warning
        // see above.
        snprintf(long_name, 64, "%s[%i]", name, j);
        int location = glGetUniformLocation(id, long_name);
        gl_log(" %i | type:%s name%s location:%i\n", i,
               gl_type_to_string(type), long_name, location);
      }
    } else {
      int location = glGetUniformLocation(id, name);
      gl_log(" %i | type:%s name:%s location:%i\n", i,
             gl_type_to_string(type), name, location);
    }
  }
}

bool gl_shader_program_is_valid(GLuint id) {
  glValidateProgram(id);
  int params = -1;
  glGetProgramiv(id, GL_VALIDATE_STATUS, &params);
  gl_log("program %i GL_VALIDATE_STATUS = %i\n", id, params);
  if(GL_TRUE != params) {
    gl_shader_program_print_info(id);
    return false;
  }
  return true;
}
