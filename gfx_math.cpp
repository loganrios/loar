#include "gfx_math.hpp"

#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>

vec2::vec2() {}

vec2::vec2(float x, float y) {
  v[0] = x;
  v[1] = y;
}

vec3::vec3() {}

vec3::vec3(float x, float y, float z) {
  v[0] = x;
  v[1] = y;
  v[2] = z;
}

vec3::vec3(const vec2& vv, float z) {
  v[0] = vv.v[0];
  v[1] = vv.v[1];
  v[2] = z;
}

vec3::vec3(const vec4& vv) {
  v[0] = vv.v[0];
  v[1] = vv.v[1];
  v[2] = vv.v[2];
}

vec4::vec4() {}

vec4::vec4(float x, float y, float z, float w) {
  v[0] = x;
  v[1] = y;
  v[2] = z;
  v[3] = w;
}

vec4::vec4(const vec2& vv, float z, float w) {
  v[0] = vv.v[0];
  v[1] = vv.v[1];
  v[2] = z;
  v[3] = w;
}

vec4::vec4(const vec3& vv, float w) {
  v[0] = vv.v[0];
  v[1] = vv.v[1];
  v[2] = vv.v[2];
  v[3] = w;
}

mat3::mat3() {}

// note: entered in COLUMNS
mat3::mat3(float a, float b, float c,
           float d, float e, float f,
           float g, float h, float i) {
  m[0] = a;
  m[1] = b;
  m[2] = c;
  m[3] = d;
  m[4] = e;
  m[5] = f;
  m[6] = g;
  m[7] = h;
  m[8] = i;
}

mat4::mat4(float a, float b, float c, float d,
           float e, float f, float g, float h,
           float i, float j, float k, float l,
           float mm, float n, float o, float p) {
  m[0]  = a;
  m[1]  = b;
  m[2]  = c;
  m[3]  = d;
  m[4]  = e;
  m[5]  = f;
  m[6]  = g;
  m[7]  = h;
  m[8]  = i;
  m[9]  = j;
  m[10] = k;
  m[11] = l;
  m[12] = mm;
  m[13] = n;
  m[14] = o;
  m[15] = p;
}

void print(const vec2& v) { printf("[%.2f, %.2f]\n", v.v[0], v.v[1]); }

void print(const vec3& v) { printf("[%.2f, %.2f, %.2f]\n", v.v[0], v.v[1], v.v[2]); }

void print(const vec4& v) { printf("[%.2f, %.2f, %.2f, %.2f]\n", v.v[0], v.v[1], v.v[2], v.v[3]); }

void print(const mat3& m) {
  printf( "\n" );
  printf( "[%.2f][%.2f][%.2f]\n", m.m[0], m.m[3], m.m[6]);
  printf( "[%.2f][%.2f][%.2f]\n", m.m[1], m.m[4], m.m[7]);
  printf( "[%.2f][%.2f][%.2f]\n", m.m[2], m.m[5], m.m[8]);
}

void print(const mat4& m) {
  printf("\n");
  printf("[%.2f][%.2f][%.2f][%.2f]\n", m.m[0], m.m[4], m.m[8], m.m[12]);
  printf("[%.2f][%.2f][%.2f][%.2f]\n", m.m[1], m.m[5], m.m[9], m.m[13]);
  printf("[%.2f][%.2f][%.2f][%.2f]\n", m.m[2], m.m[6], m.m[10], m.m[14]);
  printf("[%.2f][%.2f][%.2f][%.2f]\n", m.m[3], m.m[7], m.m[11], m.m[15]);
}

float length(const vec3& v) {
  return sqrt(v.v[0] * v.v[0] + v.v[1] * v.v[1] + v.v[2] * v.v[2]);
}

// squared length
float length2(const vec3& v) {
  return v.v[0] * v.v[0] + v.v[1] * v.v[1] + v.v[2] * v.v[2];
}

vec3 normalize(const vec3& v) {
  vec3 vb;
  float l = length(v);
  if (0.0f == l) { return vec3(0.0f, 0.0f, 0.0f); }
  vb.v[0] = v.v[0] / l;
  vb.v[1] = v.v[1] / l;
  vb.v[2] = v.v[2] / l;
  return vb;
}

vec3 vec3::operator+(const vec3& rhs) {
  vec3 vc;
  vc.v[0] = v[0] + rhs.v[0];
  vc.v[1] = v[1] + rhs.v[1];
  vc.v[2] = v[2] + rhs.v[2];
  return vc;
}

vec3& vec3::operator+=(const vec3& rhs) {
  v[0] += rhs.v[0];
  v[1] += rhs.v[1];
  v[2] += rhs.v[2];
  return *this;
}

vec3 vec3::operator-(const vec3& rhs) {
  vec3 vc;
  vc.v[0] = v[0] - rhs.v[0];
  vc.v[1] = v[1] - rhs.v[1];
  vc.v[2] = v[2] - rhs.v[2];
  return vc;
}

vec3& vec3::operator-=(const vec3& rhs) {
  v[0] -= rhs.v[0];
  v[1] -= rhs.v[1];
  v[2] -= rhs.v[2];
  return *this;
}

vec3 vec3::operator+(float rhs) {
  vec3 vc;
  vc.v[0] = v[0] + rhs;
  vc.v[1] = v[1] + rhs;
  vc.v[2] = v[2] + rhs;
  return vc;
}

vec3 vec3::operator-(float rhs) {
  vec3 vc;
  vc.v[0] = v[0] - rhs;
  vc.v[1] = v[1] - rhs;
  vc.v[2] = v[2] - rhs;
  return vc;
}

vec3 vec3::operator*(float rhs) {
  vec3 vc;
  vc.v[0] = v[0] * rhs;
  vc.v[1] = v[1] * rhs;
  vc.v[2] = v[2] * rhs;
}

vec3 vec3::operator/(float rhs) {
  vec3 vc;
  vc.v[0] = v[0] / rhs;
  vc.v[1] = v[1] / rhs;
  vc.v[2] = v[2] / rhs;
}

vec3& vec3::operator*=(float rhs) {
  v[0] = v[0] * rhs;
  v[1] = v[0] * rhs;
  v[2] = v[0] * rhs;
  return *this;
}

vec3& vec3::operator=(const vec3& rhs) {
  v[0] = rhs.v[0];
  v[1] = rhs.v[1];
  v[2] = rhs.v[2];
  return *this;
}

float dot(const vec3& a, const vec3& b) {
  return a.v[0] * b.v[0] + a.v[1] * b.v[1] + a.v[2] * b.v[2];
}

vec3 cross(const vec3& a, const vec3& b) {
  float x = a.v[1] * b.v[2] - a.v[2] * b.v[1];
  float y = a.v[2] * b.v[0] - a.v[0] * b.v[2];
  float z = a.v[0] * b.v[1] - a.v[1] * b.v[0];
  return vec3(x, y, z);
}
